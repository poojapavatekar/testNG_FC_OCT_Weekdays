package annotations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A2 {

	
	@BeforeClass
	public void beforeclass(){
		System.out.println("In before class -- a2 ");
	}
	
	@AfterSuite
	public void aftersuite(){
		System.out.println("In after suite  -- a2 ");
	}
	
	
	
	@Test(groups={"smoke","regression"})
	public void test4(){
		System.out.println("In test 4");
	}
	
	@Test(groups={"regression"})
	public void test5(){
		System.out.println("In test 5");
	}
	

	@Test(groups={"regression"})
	public void test6(){
		System.out.println("In test 6");
	}
	
	
	
}
