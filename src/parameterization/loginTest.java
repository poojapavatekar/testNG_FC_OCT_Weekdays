package parameterization;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class loginTest {

	
	@Test(dataProvider="getData",groups={"smoke","regression"})
	public void login(String username, String pwd){
		// go to login page
		//enter username
		//enter pwd
		//hit login
		//verify if loggedin
		
		System.out.println("Username : "+ username);
		System.out.println("Password : "+ pwd);
		System.out.println("************************************");
	}
	
	
	@DataProvider
	public Object[][] getData(){
		
		Object[][] data= new Object[5][2];
		
		data[0][0]="U1";
		data[0][1]="P1";
		
		data[1][0]="U2";
		data[1][1]="P2";
		
		data[2][0]="U3";
		data[2][1]="P3";
		
		data[3][0]="U4";
		data[3][1]="P4";
		
		data[4][0]="U5";
		data[4][1]="P5";
		
		
		return data;
		
		
	}
	
	

}
