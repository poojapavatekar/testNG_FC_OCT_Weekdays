package assertions;

import org.testng.Assert;
import org.testng.annotations.Test;

public class A3 {

	@Test(description="Verify login with valid credentials")
	public void test7(){
		
		// selenium code - 
		//1. i will enter uname, password , click on login btn
		// fetch the pagetile after login 
		// u will always have the expected title say - Welcome To Application
		
		String extectedTitle="Welcome To Application";
		String actualTitle="Login";// fetched by ur selenium
		
		Assert.assertEquals(actualTitle, extectedTitle," the page tile did not match ");
		System.out.println("In test 7");
	}
	
	@Test(groups={"smoke","regression"})
	public void test8(){
		System.out.println("In test 8");
		
		Assert.assertTrue(true);
		Assert.assertTrue(false,"send updates check box was expected to be checked.");
	}
	

	@Test(groups={"regression"})
	public void test9(){
		System.out.println("In test 9");
	}
	
	@Test(enabled=false,groups={"regression"})
	public void test88(){
		System.out.println("In test 88");
	}
	
	
}
