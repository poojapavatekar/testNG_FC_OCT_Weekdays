package dependancy;

import org.testng.Assert;
import org.testng.annotations.Test;

public class crud {

	
	@Test(priority=1,groups="smoke")
	public void create(){
		System.out.println("creating the user");
		Assert.assertEquals("abc", "ABC");
	}
	
	@Test(priority=2, dependsOnMethods={"create"},groups="smoke")
	public void read(){
		System.out.println("reading the user data");
	}
	
	@Test(priority=3, dependsOnMethods={"create","read"},groups="regression")
	public void edit(){
		System.out.println("editing the user");
	}
	
	@Test(priority=4, dependsOnMethods="create",groups="smoke")
	public void delete(){
		System.out.println("deleting the user");
	}
	

	
}
